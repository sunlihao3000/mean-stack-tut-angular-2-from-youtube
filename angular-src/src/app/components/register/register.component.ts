import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate.service';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService} from 'angular2-flash-messages';
import { Router } from '@angular/router';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  name:String;
  username:String;
  email:String;
  password:String;

  constructor(
    private validateSerive: ValidateService, 
    private authService:AuthService,
    private flashMessage:FlashMessagesService,
    private router:Router
  ) { }

  ngOnInit() {
  }
  
  onRegisterSubmit(){
    //console.log(1234);
    //console.log(this.name);
    const user = {
      name: this.name,
      email: this.email,
      username: this.username,
      password: this.password
    }

    // required fields
    if(!this.validateSerive.valdateRegister(user)){
      //console.log('please fill in all the fields');
      this.flashMessage.show('please fill all the fields', {cssClass:'alert-danger', timeout:3000});
      return false;
    }

    if (!this.validateSerive.validateEmail(user.email)){
      //console.log('please fill a validate email');
      this.flashMessage.show('please fill a validate email', {cssClass:'alert-danger', timeout:3000});      
      return false;
    }
    // register user
    this.authService.registerUser(user).subscribe(data =>{
      if(data.success){
        this.flashMessage.show('your are now registered and can log in',{cssClass:'alert-success', timeout:3000});
        this.router.navigate(['/login']);
      } else {
        this.flashMessage.show('something went wrong',{cssClass:'alert-danger', timeout:3000});
        this.router.navigate(['/register']);
      }
    });

  }

}

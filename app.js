const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');

// Connect to Database
mongoose.connect(config.database);

//on Connection
mongoose.connection.on('connected', () => {
    console.log('Connected to database '+ config.database);
});

mongoose.connection.on('error', (err) => {
    console.log('Database error '+ err);
});

const app = express();

mongoose.connect();

const users = require('./routes/users');

// port number
const port = 3000;

// cors middleware
app.use(cors());

//set static folder
app.use(express.static(path.join(__dirname, 'public')));

// Nody Parse Middleware
app.use(bodyParser.json());

// postport middleware
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

app.use('/users', users);

// index route
app.get('/',(req, res) =>{
    res.send('invalid Endpoint');
});

//start server
app.listen(port, () => {
    console.log('server start on port ' + port)
});